<?php

namespace Database\Seeders;

use App\Models\Article;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    protected string $model = Article::class;

    public function run(): void
    {
        Article::factory(100)->create();
    }
}
