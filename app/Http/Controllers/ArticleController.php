<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleIndexRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;

class ArticleController extends Controller
{
    public function index(ArticleIndexRequest $request)
    {
        return ArticleResource::collection(
            Article::search($request->input('q'))->get()
        );
    }
}
